# Operations

[Home](../index.md)

Running ops is non-trivial even for very low targets (like, say, 1 good hire per month). The massive fraud rate in the market means you will be bombarded with junk leads at the top of the funnel that you will have to filter out, and this is operationally very intensive and challenging. Treat it as an operations research type problem from Day 1 and you will be fine.

Look at both avg and p90 for metrics - it's often the outliers you need to worry about most.

* Common failure modes of interview pipelines
  * Not enough panelists
  * Burned out panelists
  * Sub par hires making it through
  * Rejections for wrong reasons (usually a 'creative interviewer' type problem)
  * Bad glassdoor reviews
  * Frequent reschedules of interviews by panelists
  * Process takes more than 2 weeks end to end
* Metrics
  * Lead to offer cycle time
  * Qualified leads per week
  * Number of referal leads from rejected candidates (easiest way to figure out if candidates are having a good experience - are the candidates we reject recommending us to their friends?)
  * Number of candidates currently experiencing broken SLAs
  * High quality hires per month (accept offer and show)
  * Offer reject %age for last 2 months rolling
  * Offer bounce rate (accept offer then back out or no show)
  * Number of negative glassdoor reviews related to interview process rolling over six months
  * Panelist utilization (are we overloaded? how well distributed is the load?)
  * Number of rejected candidates who have *not* gotten feedback around their rejection 



## Recommended Reading

1. "The Goal", Goldratt